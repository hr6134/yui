#!/usr/bin/env python

import sys
import os
import shutil
import sqlite3
import urllib
import commands

# 1: db path
# 2: filter string 
# 3: target
# keys can be placed anywhere
# db_path can be omited

rating_filter = 1
db_path = ''
filter_string = ''
target_path = ''
user_name = commands.getoutput('whoami')
if os.uname()[0] == 'Linux':
    db_path_search_list = commands.getoutput('find /home/' + user_name + '/.songbird2/ -name "main*"').split('\n')
if os.uname()[0] == 'Darwin':
    db_path_search_list = commands.getoutput('find /Users/' + user_name + '/Library/Application\ Support/Songbird2/Profiles/ -name "main*"').split('\n')
db_path = db_path_search_list[0]

for i in sys.argv[1:]:
    if i == '-r':
        remove = True
        sys.argv.remove(i)
    if i.startswith('-t'):
        rating_filter = i[2:]
        sys.argv.remove(i)

if len(sys.argv) == 4:
    db_path = sys.argv[1]
    filter_string = sys.argv[2]
    target_path = sys.argv[3]
if len(sys.argv) == 3:
    filter_string = sys.argv[1]
    target_path = sys.argv[2]

if remove:
    for i in os.listdir(target_path):
        os.remove(target_path + i)

cursor = sqlite3.connect(db_path).cursor()
cursor.execute("select * from properties where property_name like '%rating%'")
rating_id = cursor.fetchone()[0]

cursor.execute("select * from resource_properties")
tmp = cursor.fetchall()
tracks = [str(x[0]) for x in tmp if x[1] == rating_id and x[2] >= rating_filter]
cursor.execute("select content_url from media_items where media_item_id in (%s)" % ', '.join(tracks))
# why it is faster with tmp
tmp = cursor.fetchall()
track_paths = [urllib.unquote_plus(x[0]) for x in tmp if x[0].lower().find(filter_string.lower()) != -1]
#track_paths = [x[0] for x in cursor.fetchall() if x[0].lower().find(sys.argv[2]) != -1]

for i in track_paths:
    print i[7:]

for i in track_paths:
    file_name = i[7:]
    shutil.copyfile(file_name, target_path + file_name.split('/')[len(file_name.split('/')) - 1])
